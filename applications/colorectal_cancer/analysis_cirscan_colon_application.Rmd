---
title: "Application of Cirscan on the colon public dataset - GSE126095"
author: "YB & RMF"
date: "`r Sys.Date()`"
output: 
  rmarkdown::html_document:
    toc: true
    toc_float: true
    toc_depth: 3
    number_sections: true
---

# Load packages
```{r}
library(readr)
library(data.table)
library(fgsea)
```

# Application using the colon miRNA matrix
## Public colon sponge mechanisms
```{r, message = FALSE}
colon_sponge_mechanisms_annot <- read_delim("literature_CRC_circrna_sponge_cirscan.csv", delim = ";", escape_double = FALSE, trim_ws = TRUE) # from NcPath (10.1093/bioinformatics/btac812) and 10.1038/s41388-019-0857-8 
colon_sponge_mechanisms_annot <- as.data.frame(colon_sponge_mechanisms_annot)
```

```{r}
colnames(colon_sponge_mechanisms_annot)
dim(colon_sponge_mechanisms_annot)
```

## Cirscan output using the colon miRNA matrix 
```{r}
cirscan_output <- fread("supplementary_table_1.csv", data.table = F)
cirscan_output <- as.data.frame(cirscan_output)
```

```{r}
colnames(cirscan_output)
dim(cirscan_output)
length(unique(cirscan_output$target))
```

## Intersect public colon sponge mechanisms and Cirscan interaction score matrix
```{r}  
# public colon sponge mechanisms
colon_sponge_mechanisms_annot$sponge <- paste(colon_sponge_mechanisms_annot$circbase_id, colon_sponge_mechanisms_annot$miRNA, sep = "_")
head(colon_sponge_mechanisms_annot[ ,"sponge"])
dim(colon_sponge_mechanisms_annot)

# interaction score matrices
cirscan_output$sponge <- paste(cirscan_output$target, cirscan_output$miRNA, sep = "_")
rownames(cirscan_output) <- cirscan_output$sponge

# intersect
intersect_sponge_cirscan <- intersect(colon_sponge_mechanisms_annot$sponge, cirscan_output$sponge)
length(intersect_sponge_cirscan)
intersect_sponge_cirscan

cirscan_output[cirscan_output$sponge %in% intersect_sponge_cirscan, 1:5]
```

## Check the ranks for the recovered sponge mechanisms in each condition
```{r}  
spongecommon = intersect(rownames(cirscan_output), intersect_sponge_cirscan)
cirscan_output[spongecommon, 1:4]

dim(cirscan_output)
cirscan_outputCond1 = cirscan_output[which(cirscan_output$conditions=="condition_1"),]
dim(cirscan_outputCond1)
cirscan_outputCond1$Rank1 = 1:nrow(cirscan_outputCond1)
cirscan_outputCond2 = cirscan_output[which(cirscan_output$conditions=="condition_2"),]
dim(cirscan_outputCond2)
cirscan_outputCond2$Rank2 = 1:nrow(cirscan_outputCond2)

cirscan_outputCond1[spongecommon, "Rank1"]
cirscan_outputCond2[spongecommon, "Rank2"]
```

## Test the significance of the cirscan ranking of the recovered sponge mechanisms
```{r}  
# scores
pathways = list(sponge_colon = colon_sponge_mechanisms_annot$sponge)
circ_mir_spongescore = cirscan_output$ss_mirna_circrna_interaction
names(circ_mir_spongescore)  = rownames(cirscan_output)
fgseaRes = data.frame(fgsea(pathways, circ_mir_spongescore, minSize=1, maxSize=100,scoreType = "pos"))
fgseaRes

######
fgseaRes$pval
######
```

## Top 10 circrna candidates in tumoral condition
```{r}
cirscan_output_tumoral <- cirscan_output[cirscan_output$conditions == "condition_2", ]
unique(cirscan_output_tumoral$target)[1:10]
```

# Application using the colon TCGA signature
## Load cirscan output using the colon TCGA matrix
```{r}
cirscan_output <- fread("supplementary_table_2.csv", data.table = F)
cirscan_output <- as.data.frame(cirscan_output)
```

```{r}
dim(cirscan_output)
colnames(cirscan_output)
```

## Intersect public colon sponge mechanisms and Cirscan interaction score matrix
```{r}  
# public colon sponge mechanisms
dim(colon_sponge_mechanisms_annot)

# interaction score matrices
cirscan_output$sponge <- paste(cirscan_output$target, cirscan_output$miRNA, sep = "_")
rownames(cirscan_output) <- cirscan_output$sponge

# intersect
intersect_sponge_cirscan <- intersect(colon_sponge_mechanisms_annot$sponge, cirscan_output$sponge)
length(intersect_sponge_cirscan)
intersect_sponge_cirscan 

cirscan_output[cirscan_output$sponge %in% intersect_sponge_cirscan, 1:4]
```

## Check the ranks for the recovered sponge mechanisms in each condition
```{r}  
spongecommon = intersect(rownames(cirscan_output), intersect_sponge_cirscan)
cirscan_output[spongecommon, 1:4]

dim(cirscan_output)
cirscan_outputCond1 = cirscan_output[which(cirscan_output$conditions=="condition_1"),]
dim(cirscan_outputCond1)
cirscan_outputCond1$Rank1 = 1:nrow(cirscan_outputCond1)
cirscan_outputCond2 = cirscan_output[which(cirscan_output$conditions=="condition_2"),]
dim(cirscan_outputCond2)
cirscan_outputCond2$Rank2 = 1:nrow(cirscan_outputCond2)

cirscan_outputCond1[spongecommon, "Rank1"]
cirscan_outputCond2[spongecommon, "Rank2"]
```

## Test the significance of the cirscan ranking of the recovered sponge mechanisms
```{r}
# score
pathways = list(sponge_colon = colon_sponge_mechanisms_annot$sponge)
circ_mir_spongescore = cirscan_output$ss_mirna_circrna_interaction
names(circ_mir_spongescore)  = rownames(cirscan_output)
fgseaRes = data.frame(fgsea(pathways, circ_mir_spongescore, minSize=1, maxSize=100, scoreType = "pos"))
fgseaRes

fgseaRes$pval
```

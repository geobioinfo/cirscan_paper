
<img src="www/black_cirscan_logo.png" width="300">

## **A shiny application to identify differentially active sponge mechanisms and visualize circRNA-miRNA-mRNA networks.**

## **Overview**

Script used to analyse the results of the Cirscan tool on colorectal
cancer and hepatocarcinoma data.

## **Cirscan tool**

The RShiny Cirscan tool is available at this
[link](https://gitlab.com/geobioinfo/cirscan_Rshiny).

## **About**
## **About**

Fraboulet, RM., Si Ahmed, Y., Aubry, M. et al. Cirscan: a shiny
application to identify differentially active sponge mechanisms and
visualize circRNA-miRNA-mRNA networks. BMC Bioinformatics 25, 53 (2024).
<https://doi.org/10.1186/s12859-024-05668-y>

Institute Genetics & Development of Rennes (IGDR) UMR 6290 CNRS - UR
<br> Gene Expression and Oncogenesis team (GEO)

*Rose-Marie FRABOULET and Dr.Yuna BLUM*
